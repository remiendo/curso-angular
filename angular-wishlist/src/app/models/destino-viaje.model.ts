import {v4 as uuid} from 'uuid';

export class DestinoViaje {
	 Selected: boolean;
	 servicios: string[];
    id = uuid();
   public votes = 0;

    constructor(public nombre: string, public imagenUrl: string) { 
    	this.servicios = ['Pileta', 'Desayuno'];
    }
    setSelected(s: boolean) {
    	this.Selected = s;
    }

    isSelected(): boolean {
    	return this.Selected;

    }

    voteUp() {
    	this.votes++;
    }
    voteDown() {
    	this.votes--;
    }
}